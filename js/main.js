
var $getBatchForm = $('#getBatchForm');
var $userId = $('#userId');
var $projectName = $('#projectName');
var $getBatch = $('#getBatch');
var $batchError = $('#batchError');

var $loginForm = $('#loginForm');
var $username = $('#username');
var $password = $('#password');
var $login = $('#login');
var $loginError = $('#loginError');

var gettingBatch = false;
var loggingIn = false;

$(document).ready(function() {
    $getBatchForm.on('submit', function(e) {
        getBatch(e);
    });
    $loginForm.on('submit', function(e) {
        e.preventDefault();
        login();
    });
});

function login() {
    if(loggingIn) { return false; }

    loggingIn = true;
    // hide the error message
    $loginError.addClass('hidden');

    // update UI state of the button
    $login.val('Loading').addClass('disabled');

    let data = {
        'username': $username.val(),
        'password': $password.val()
    }
    
    $.ajax({
        url: 'functions/login-auth.php',
        type: 'POST',
        data: data,
        success: function(data, status) {

            if(data === 'invalid') {
                $loginError.removeClass('hidden').text('Invalid username/password');
            } else {
                if(data === '') {
                    window.location = ROOT;
                } else {
                    window.location = data;
                }
            }

            // reset the UI state of the button back
            // to its original state
            $login.val('Login').removeClass('disabled');

            loggingIn = false;
        },
        error: function(xhr, desc, err) {
            console.log(xhr);
            console.log("Details: " + desc + "\nError: " + err);
        }
    });
}

function getBatch(e) {
    e.preventDefault();
    if(gettingBatch) { return false; }

    gettingBatch = true;
    // hide the error message
    $batchError.addClass('hidden');

    // update UI state of the button
    $getBatch.val('Loading').addClass('disabled');
    
    // prepare the data
    let userId = $userId.val();
    let projectName = $projectName.val();
    let data = {
        'user_id': userId,
        'project_name': projectName,
        'mode': $('.mode').val(),
        'server': $('input[name=server]:checked').val()
    }

    $.ajax({
        url: 'functions/get-batch.php',
        type: 'POST',
        data: data,
        success: function(data, status) {

            if(data === 'success') {
                // redirect to the working page
                window.location = ROOT + 'working.php';
            } else if(data === 'batch_not_ok') {
                $batchError.removeClass('hidden').text('There was an error getting the batch');
            } else if(data === 'batch_not_found') {
                $batchError.removeClass('hidden').text('Batch not found');
            }

            // reset the UI state of the button back
            // to its original state
            $getBatch.val('Get Batch').removeClass('disabled');

            gettingBatch = false;
        },
        error: function(xhr, desc, err) {
            console.log(xhr);
            console.log("Details: " + desc + "\nError: " + err);
        }
    });
}