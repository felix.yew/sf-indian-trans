var audio_details = (function(segmentsHandler) {

    // all in seconds
    var total_duration = -1;
    var effective_duration = -1;

    // cache DOM
    var $totalDuration = $("#totalDuration");
    var $effectiveDuration = $("#effectiveDuration");

    _init();

    function _init() {
        total_duration = _get_seconds_from_string($totalDuration.text());
        
        _bindEvents();
    }

    function _render() {
        $effectiveDuration.text(_get_string_from_seconds(effective_duration));
    }
    
    function _bindEvents() {
        $(document).ready(function() {
            let segmentString = segmentsHandler.getSegmentsString();
            let total_invalid = 0;
            for(let i = 0; i < segmentString.length; i++) {
                if(segmentString[i].label === '' || segmentString[i].label === 'C') {
                    total_invalid += segmentString[i].duration;
                }
            }
            
            effective_duration = parseInt(total_duration - total_invalid);
            _render();
        });

        events.on('segmentsUpdated', _updateEffectiveDuration);
    }

    function _updateEffectiveDuration(segmentString) {
        let total_invalid = 0;
        for(let i = 0; i < segmentString.length; i++) {
            if(segmentString[i].label === '' || segmentString[i].label === 'C') {
                total_invalid += segmentString[i].duration;
            }
        }

        effective_duration = parseInt(total_duration - total_invalid);
        _render();
    }

    function _get_seconds_from_string(val) {
        let tokens = val.split(":");

        return (parseInt(tokens[0]) * 3600) + (parseInt(tokens[1]) * 60) + parseInt(tokens[2]);
    }

    function _get_string_from_seconds(val) {
        let hours = 0, minutes = 0, seconds = 0;

        hours = Math.floor(val / 3600);
        val -= (hours * 3600);

        minutes = Math.floor(val / 60);
        val -= (minutes * 60);

        seconds = val;

        return _pad(hours) + ":" + _pad(minutes) + ":" + _pad(seconds);
    }

    function _pad(n){
        return n > 9 ? "" + n: "0" + n;
    }
    
})(segmentsHandler);