
var inputManager = (function() {

    var $contentValueInput = $('#contentValueInput');
    var $labelValueInput = $('#labelValueInput');

    var $errorMessage = $('#errorMessage');
    var $qcProblems = $('input[type=checkbox]');

    _init();

    function _init() {
        _bindEvents();
    }

    function _bindEvents() {
        events.on('segmentSelected', _setInputValues);
        events.on('errorUpdated', _setErrorMessage);
        events.on('contentUpdated', _setInputValues);

        $contentValueInput.on('change paste keyup', _updateContent);
        $labelValueInput.on('change', _updateLabel);

        $contentValueInput.on('focusin', function() {
            events.emit('contentValueFocus', true);
        });
        $contentValueInput.on('focusout', function() {
            events.emit('contentValueFocus', false);
        });

        if(qc) {
            $qcProblems.on('change', function() {
                var searchIDs = $("input[type=checkbox]:checked").map(function(){
                    return $(this).val();
                }).get();
                events.emit('qcErrorChanged', searchIDs);
            });
        }
    }

    function _setInputValues(data) {
        if(data.segments.length > 0) {
            $contentValueInput.val(data.segments[data.selectedSegmentIndex].content);
            $labelValueInput.val(data.segments[data.selectedSegmentIndex].label);

            $contentValueInput.focus();
            _setErrorMessage(data.error);

            if(qc) {
                let qc_errors = data.segments[data.selectedSegmentIndex].qcErrors.split(',');
                $('input[type=checkbox]').prop('checked', false);
                for(let i = 0; i < qc_errors.length; i++) {
                    $('input[type=checkbox][value="' + qc_errors[i] + '"]').prop('checked', true);
                }
            }
        }
    }

    function _setErrorMessage(error) {
        if(!error) {
            $errorMessage.addClass('text-success')
                         .removeClass('text-danger')
                         .text('No errors');
        } else {
            $errorMessage.addClass('text-danger')
                         .removeClass('text-success')
                         .text(error);
        }
    }

    function _updateContent() {
        let val = $contentValueInput.val();
        val = val.replace(/[^A-Za-z\'-\s]/g,"").replace(/  +/g, ' ');

        if(val === ' ' && val.length === 1) {
            val = '';
        }
        $contentValueInput.val(val);

        events.emit('contentValueChanged', $contentValueInput.val())
    }

    function _updateLabel() {
        events.emit('labelValueChanged', $labelValueInput.val())
    }
})();