
var magnifier = (function() {

    var STEPS = 10;

    var ZOOM_IN = 'zoomIn';
    var ZOOM_OUT = 'zoomOut';

    var $zoomIn = $('#zoomIn');
    var $zoomOut = $('#zoomOut');

    var zoomValue = 30;

    _init();

    function _init() {
        _bindEvents();
    }

    function _bindEvents() {
        $zoomIn.on('click', function() {
            _updateZoom(ZOOM_IN);
        });
        $zoomOut.on('click', function() {
            _updateZoom(ZOOM_OUT);
        });

        events.on('hotkeyZoomIn', () => {
            _updateZoom(ZOOM_IN);
        });
        events.on('hotkeyZoomOut', () => {
            _updateZoom(ZOOM_OUT);
        });
    }

    function _updateZoom(mode) {
        if(mode === ZOOM_IN) {
            if(zoomValue === 200) { return false; }
            zoomValue += STEPS;
        } else if(mode === ZOOM_OUT) {
            if(zoomValue <= 0) { return false; }
            zoomValue -= STEPS;
        }

        console.log(zoomValue);
        events.emit('zoomUpdated', {
            'mode': mode,
            'value': zoomValue
        });
    }
})();