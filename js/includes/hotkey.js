var hotkey = (function() {

    var altKeyHeld = false;
    var shiftKeyHeld = false;

    var map = {};

    _init();

    function _init() {

        onkeydown = onkeyup = function(e){
            e = e || event; // to deal with IE

            if(e.keyCode == '18') {
                if(e.type == 'keyup') {
                    altKeyHeld = false;
                } else {
                    altKeyHeld = true;
                }
            } else if(e.keyCode == '16') {
                if(e.type == 'keyup') {
                    shiftKeyHeld = false;
                } else {
                    shiftKeyHeld = true;
                }
            }

            map[e.keyCode] = e.type == 'keydown';

            _segmentDelete(map);
            _zooming(map);
            _segmentSelection(map);
            _segmentResizing(map);
        }
    }

    function _segmentDelete(map) {
        if(map[18] && (map[8] || map[46])) {
            map[18] = 0;
            map[46] = 0;
            map[8] = 0;

            events.emit('hotkeyDelete', true);
        }
    }

    function _zooming(map) {
        if(altKeyHeld) {
            if(map[73]) {
                map[73] = 0;
                events.emit('hotkeyZoomIn', true);
            } else if(map[79]) {
                map[79] = 0;
                events.emit('hotkeyZoomOut', true);
            }
        }
    }

    function _segmentSelection(map) {
        if(altKeyHeld && !shiftKeyHeld) {
            if(map[37]) {
                map[37] = 0;
                events.emit('hotkeySelectLeft', true);
            } else if(map[39]) {
                map[39] = 0;
                events.emit('hotkeySelectRight', true);
            }
        }
    }

    function _segmentResizing(map) {
        if(altKeyHeld && shiftKeyHeld) {
            if(map[37]) {
                map[37] = 0;
                events.emit('hotkeyResizeLeft', true);
            } else if(map[39]) {
                map[39] = 0;
                events.emit('hotkeyResizeRight', true);
            }
        }
    }

})();