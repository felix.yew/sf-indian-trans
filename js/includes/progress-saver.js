var progressSaver = (function(segmentsHandler, timeSegmentsHandler) {

    $saveStatus = $('#saveStatus');
    
    _init();

    function _init() {
        // saves every 1 minute
        setInterval(_saveProgress, 60000);

        _bindEvents();
    }

    function _bindEvents() {
        events.on('save', _saveProgress);
    }

    function _saveProgress() {
        $saveStatus.removeClass('hidden');
        $saveStatus.text('Saving').addClass('text-info');

        localStorage.setItem(workingKey, JSON.stringify(
            segmentsHandler.getSegmentsString()
        ));

        $saveStatus.text('Saved').addClass('text-success');
        setTimeout(() => {
            $saveStatus.addClass('hidden');
        }, 10000);
    }

})(segmentsHandler, timeSegmentsHandler);