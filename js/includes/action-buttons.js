
var actionButtons = (function(segmentsHandler) {

    var FAST_FORWARD = 'fastForward';
    var FAST_BACKWARD = 'fastBackward';

    var SPEED_UPDATE_STEP = 0.10;

    var playing = false;
    var currentPlaySpeed = 1;

    var submittingBatch = false;

    var $playPauseButton = $('#playPauseButton');
    var $playbackControl = $('#playbackControl');

    var $fastBackward = $('#fastBackward');
    var $fastForward = $('#fastForward');
    var $speed = $('#speed');
    var $saveProgress = $('#saveProgress');

    var $submitBatch = $('#submitBatch');
    var $failBatch = $('#failBatch');

    _init();

    function _render() {
        if(playing) {
            $playPauseButton.text('Pause');
        } else {
            $playPauseButton.text('Play');
        }

        if(currentPlaySpeed === 1) {
            $speed.text('Normal');
        } else {
            $speed.text(currentPlaySpeed.toFixed(2));
        }
    }

    function _init() {
        _bindEvents();
    }

    function _bindEvents() {
        $playPauseButton.on('click', function() {
            events.emit('togglePlayPause', true);
        });

        events.on('isPlaying', _togglePlayPause);

        $fastForward.on('click', function() {
            _updateSpeed(FAST_FORWARD);
        });
        $fastBackward.on('click', function() {
            _updateSpeed(FAST_BACKWARD);
        });

        $submitBatch.on('click', () => {
            _submitBatch(false);
        });
        $failBatch.on('click', () => {
            _submitBatch(true);
        });
        $saveProgress.on('click', () => events.emit('save'));
    }

    function _submitBatch(fail) {
        if(submittingBatch) { return false; }

        let confirmMessage = fail ? 'Are you sure you want to FAIL this batch?' : 'Are you sure you want to submit this batch?';
        if(!confirm(confirmMessage)) {
            return false;
        }

        // perform checking on all segments first
        let segments = segmentsHandler.getSegmentsString();
        let success = _checkSegments(segments);

        if(!success && !fail && !isAdmin) {
            alert('There are still some segments that have an error.');
            return false;
        }

        success = _checkSampingRate();
        if(!success && !fail && !isAdmin) {
            alert('Sampled percentage is still lacking.');
            return false;
        }
        
        if(fail) {
            $failBatch.text('Failing').addClass('disabled');
            $submitBatch.addClass('disabled');
        } else {
            $failBatch.addClass('disabled');
            $submitBatch.text('Submitting').addClass('disabled');
        }

        submittingBatch = true;

        let data = {
            'segments': segments
        }
        
        let pass_or_fail = true;
        if(qc === 'qc.php') {
            let passingRate = parseInt($('#passingRate').text());
            pass_or_fail = (passingRate >= 97);
        }
        if(fail) {
            pass_or_fail = false;
        }
        if(isAdmin) {
            pass_or_fail = true;
        }

        let effective_duration = $('#effectiveDuration').text();

        $.ajax({
			url: 'functions/submit-results.php',
			type: 'POST',
			data: {
                data: JSON.stringify(data),
                pass_or_fail: pass_or_fail,
                effective_duration: effective_duration
            },
			success: function(data, status) {
				console.log(data);

				// reset the submit button to its
				// default state
                $submitBatch.text('Submit').removeClass('disabled');
                $failBatch.text('Fail').removeClass('disabled');

				if(data === 'success') {
					alert('Batch successfully submitted.');

                    localStorage.removeItem(workingKey);

					window.location = ROOT + qc;
				} else {
					alert(data);
                }
                
                submittingBatch = false;
			},
			error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError: " + err);
			}
		});
    }

    function _updateSpeed(mode) {
        if(mode === FAST_FORWARD) {
            if(currentPlaySpeed < 1.3) {
                currentPlaySpeed += SPEED_UPDATE_STEP;
            }
        } else if(mode === FAST_BACKWARD) {
            if(currentPlaySpeed > 0.8) {
                currentPlaySpeed -= SPEED_UPDATE_STEP;
            }
        }

        _render();
        events.emit('playbackSpeedUpdate', currentPlaySpeed * 100);
    }
    
    function _checkSegments(segments) {
        let success = true;

        for(let i = 0; i < segments.length; i++) {
            if(segments[i].withError) {
                success = false;
                break;
            }
        }

        return success;
    }
    
    function _checkSampingRate() {
        if(qc === '') {
            return true;
        }

        sampledPercentage = parseInt($('#sampledPercentage').text());
        if(qc === 'qc.php') {
            if(sampledPercentage < 100) {
                return false;
            }
        } else if(qc === 'final-qc.php') {
            if(sampledPercentage < 30) {
                return false;
            }
        }

        return true;
    }

    function _togglePlayPause(data=null) {
        playing = data;
        _render();
    }
})(segmentsHandler);