var waveForm = (function(audio) {

    var wave = null;
    var cursor = null;

    var totalDuration = -1;
    var autoSeek = false;
    var currentRegion = null;
    var widthStack = [];

    var $transcriber = $('#transcriber');
    var $waveform = $('#waveform');
    var $segmentList = $('#segmentList');

    _init();

    function _init() {
        _initializeWave();
    }

    function _render() {
        if(cursor.getTimePoint() === 0) {
            cursor.hideCursor();
        } else {
            cursor.showCursor();
        }

        cursor.adjustPosition();

        wave.clearRegions();
        if(currentRegion) {
            wave.addRegion(currentRegion);
        }
    }

    function _initializeWave() {
        wave = WaveSurfer.create({
            container: '#waveform',
            progressColor: '#2F6F45',
            waveColor: '#45d876',
            responsive:true,
            minPxPerSec: 1,
            scrollParent: true,
            plugins: [
                WaveSurfer.regions.create({})
            ]
        });

        // load the audio clip
        wave.load(audio);
        
        wave.on('ready', function() {
            $('.preloader').fadeOut(500);

            totalDuration = parseFloat(wave.getDuration());
            cursor = new Cursor(0, totalDuration);
            _bindEvents();

            events.emit('waveReady', totalDuration);
            _setZoom(null, 30);
        });
    }

    function _setZoom(value=null, initial=false) {

        let zoomValue = initial ? initial : value.value;

        if(initial || value.mode === 'zoomIn') {
            width = _getWaveWidth();
            widthStack.push(width);
            wave.zoom(zoomValue);
            width = _getWaveWidth();
        } else if(value.mode === 'zoomOut') {
            width = widthStack.pop();
            $waveform.css('width', width);
            wave.zoom(zoomValue);
        }

        if(width) {
            $waveform.css('width', width);
            events.emit('widthUpdated', width);
        }
    }

    function _getWaveWidth() {
        return (wave.drawer.width / wave.params.pixelRatio) + 'px';
    }

    function _bindEvents() {
        wave.on('seek', function() {
            _moveCursor();
        });
        wave.on('finish', function() {
            _togglePlayPause(false);
        });

        $(cursor.$dropCursor).on('click', () => cursor.dropCursor());

        events.on('segmentSelected', function(data) {
            _moveCursor(data);
        });
        events.on('playAudio', _playAudio);
        events.on('segmentsResized', _updateRegion);
        events.on('zoomUpdated', _setZoom);
        events.on('togglePlayPause', _togglePlayPause);
        events.on('playbackSpeedUpdate', _updatePlaybackSpeed);
        events.on('getPlayingFlag', _sendPlayingFlag);
    }

    function _sendPlayingFlag() {
        events.emit('isPlaying', wave.isPlaying());
    }

    function _updatePlaybackSpeed(speed) {
        wave.setPlaybackRate(speed/100);
    }

    function _togglePlayPause(continue_play=true) {
        if(continue_play) {
            wave.playPause();
        }
        events.emit('isPlaying', wave.isPlaying());
    }    

    function _moveCursor(data) {
        if(autoSeek && !data) {
            autoSeek = false;
            return false;
        }

        let segmentSelection = false;
        if(data) {
            currentRegion = {
                start: data.segments[data.selectedSegmentIndex].start, // time in seconds
                end: data.segments[data.selectedSegmentIndex].end, // time in seconds
                color: 'hsla(100, 100%, 30%, 0.1)',
                drag: false,
                resize: false,
            }
            let startTime = data.segments[data.selectedSegmentIndex].start;
            autoSeek = true;

            segmentSelection = true;

            if(!data.playingFlag) {
                cursor.moveCursor(startTime);
                wave.seekTo(startTime / totalDuration);
            }
        } else {
            currentRegion = null;
            cursor.moveCursor(wave.getCurrentTime());
        }
        
        _render();

        events.emit('cursorMoved', {
            'mode': 'cursorMoved',
            'timePoint': cursor.timePoint,
            'totalDuration': cursor.totalDuration,
            'segmentSelection': segmentSelection
        });
    }

    function _updateRegion(data) {
        if(currentRegion) {
            currentRegion.start = data.segments[data.selectedSegmentIndex].start,
            currentRegion.end = data.segments[data.selectedSegmentIndex].end,

            _render();
        }
    }

    function _playAudio(data) {
        let mode = data.mode;

        if(mode === 'region') {
            let isNew = data.isNew;
            let segment = data.timeSegment;

            let currentTime = wave.getCurrentTime();
            
            if(isNew || (currentTime > segment.end)) {
                wave.stop();
                wave.play(segment.start, segment.end);

                currentRegion = {
                    start: segment.start, // time in seconds
                    end: segment.end, // time in seconds
                    color: 'hsla(100, 100%, 30%, 0.1)',
                    drag: false,
                    resize: false,
                }
                wave.clearRegions();
                wave.addRegion(currentRegion);
            } else {
                wave.playPause();
            }
        }

        _sendPlayingFlag();
    }

})(audio);