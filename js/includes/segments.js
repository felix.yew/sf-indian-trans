
var SEGMENT_OKAY_DURATION = 10;

class Segment {
    constructor(start, end, content='', label='', tested=false, qcErrors='') {
        this.start = start;
        this.end = end;
        this.content = content;
        this.label = label;
        this.tested = tested;
        this.withError = false;
        this.qcErrors = qcErrors;
    }

    getDuration() {
        return this.end - this.start;
    }

    export() {
        return {
            'start': this.start,
            'end': this.end,
            'content': this.content,
            'label': this.label,
            'duration': this.getDuration(),
            'withError': this.withError,
            'tested': this.tested,
            'qcErrors': this.qcErrors
        }
    }
}

class SegmentList {
    constructor(cachedValue) {
        this.segments = [];
        this.selectedSegmentIndex = 0;
        this.testedCount = 0;
        this.passingRate = 0;
        
        if(cachedValue !== 'null') {
            // initialize the segments array
            this._preInit(cachedValue);

            this.computePassingRate();
        }
    }

    _preInit(cachedValue) {
        let values = JSON.parse(cachedValue);
        for(var i = 0; i < values.length; i++) {
            let segment = new Segment(
                values[i].start, values[i].end,
                values[i].content, values[i].label,
                values[i].tested, values[i].qcErrors.toString()
            );
            this._getErrorMessage(segment);

            this.segments.push(segment);
        }
    }

    addSegments(cursorData) {
        // extract data from cursorData
        let timePoint = cursorData.timePoint;
        let totalDuration = cursorData.totalDuration;

        let segment1Start, segment1End, segment2Start, segment2End;
        let old_content = '', old_label = '';

        let i = 0;

        if(this.segments.length === 0) {
            segment1Start = 0;
            segment1End = timePoint;

            segment2Start = timePoint;
            segment2End = totalDuration;
        } else {
            // find the correct segment index
            while(!this._correctSegment(i, timePoint)) { i++; }

            segment1Start = this.segments[i].start;
            segment1End = timePoint;

            segment2Start = timePoint;
            segment2End = this.segments[i].end;

            // delete the current segment
            old_content = this.segments[i].content;
            old_label = this.segments[i].label;
            this.segments.splice(i, 1);
        }

        var segment1 = new Segment(segment1Start, segment1End, old_content, old_label);
        var segment2 = new Segment(segment2Start, segment2End, '', '');
        
        this._getErrorMessage(segment1);
        this._getErrorMessage(segment2);

        this.segments.splice(i, 0, segment1);
        this.segments.splice(i+1, 0, segment2);

        this.selectedSegmentIndex = i;

        this.computePassingRate();
    }

    _correctSegment(index, timePoint) {
        return timePoint >= this.segments[index].start && timePoint <= this.segments[index].end;
    }

    selectSegment(selectedSegmentIndex) {
        this.selectedSegmentIndex = selectedSegmentIndex;
        if(!this.segments[this.selectedSegmentIndex].tested) {
            this.testedCount++;
        }
        this.segments[this.selectedSegmentIndex].tested = true;

        this.computePassingRate();

        return this._getErrorMessage();
    }

    setContent(value) {
        if(this.segments.length > 0) {
            this.segments[this.selectedSegmentIndex].content = value;
        }
        this.computePassingRate();
    }

    setLabel(value) {
        if(this.segments.length > 0) {
            this.segments[this.selectedSegmentIndex].label = value;
        }
        this.computePassingRate();
    }

    deleteCurrentSegment() {
        if(this.selectedSegmentIndex === 0 || this.segments.length === 0) {
            return false;
        } else {
            // combine the the deleted segment and the previous segment
            this.segments[this.selectedSegmentIndex-1].content += ' ' +
                this.segments[this.selectedSegmentIndex].content;
            this.segments[this.selectedSegmentIndex-1].content = this.segments[this.selectedSegmentIndex-1].content.trim();
            this.segments[this.selectedSegmentIndex-1].end = this.segments[this.selectedSegmentIndex].end;
            
            // delete the segment
            this.segments.splice(this.selectedSegmentIndex, 1);

            // set the selected segment to the previous segment
            this.selectedSegmentIndex--;
            return true;
        }
    }

    exportSegments() {
        let output = [];

        this.segments.forEach(function(segment) {
            output.push(segment.export());
        });

        return output;
    }

    computePassingRate() {
        this.testedCount = 0;
        
        let passedSegments = 0;
        for(let i = 0; i < this.segments.length; i++) {
            if(this.segments[i].tested) { 
                this.testedCount++;

                if(this.segments[i].qcErrors === '' && !this._getErrorMessage(this.segments[i])) {
                    passedSegments++;
                }
            }
        }

        if(this.testedCount > 0) {
            this.passingRate = (parseFloat(passedSegments / this.testedCount) * 100).toFixed(0);
        }
    }

    updateQcErrors(data) {
        this.segments[this.selectedSegmentIndex].qcErrors = data.join(',');
        this.computePassingRate();
    }

    _getErrorMessage(segment = null) {

        segment = segment || this.segments[this.selectedSegmentIndex];
        let content = segment.content;
        let label = segment.label;
        let withError = false;
        let message = false;

        if(segment.getDuration() > SEGMENT_OKAY_DURATION) {
            message = "Segment duration must be within 10 seconds";
            withError = true;
        } else if(label === '') {
            message = "Select a label for the segment";
            withError = true;  
        } else if(content === '' && label !== 'C') {
            message = "Add content to the segment";
            withError = true;
        } else if(content !== '' && label === 'C') {
            message = "Segment is of type Invalid, so remove content";
            withError = true;
        }

        segment.withError = withError;

        return message;
    }
}

var segmentsHandler = (function() {
    
    var segmentList = null;
    var totalDuration = -1;

    var segmentListWidth = -1;

    var playingFlag = false;

    var $actualList = $('#actualList');
    var $segmentList = $('#segmentList');
    var $segmentsCount = $('#segmentsCount');
    var $sampledSegments = $('#sampledSegments');
    var $sampledPercentage = $('#sampledPercentage');
    var $passingRate = $('#passingRate');

    _init();

    function _init() {
        let cachedValue = localStorage.getItem(workingKey) || 'null';

        segmentList = new SegmentList(cachedValue);

        let qcSegmentsCount = 0;
        if(qc === 'qc.php') {
            qcSegmentsCount = segmentList.segments.length;
        } else if(qc === 'final-qc.php') {
            qcSegmentsCount = parseInt(Math.ceil(segmentList.segments.length * .3));
        }
        
        
        $segmentsCount.text(qcSegmentsCount);
        $passingRate.text(segmentList.passingRate);
        _bindEvents();
    }
    
    function _render() {
        if(segmentListWidth !== -1) {
            $segmentList.css('width', segmentListWidth);
        }
        
        var segmentsHTML = [];
        var i = 0;
        segmentList.segments.forEach(function(segment) {
            segmentsHTML.push(_createSegmentHTML(segment, i));
            i++;
        });
        $actualList.html(segmentsHTML);

        _enableHandles();

        $sampledSegments.text(segmentList.testedCount);

        let sampledPercentage = parseFloat(segmentList.testedCount / segmentList.segments.length) * 100;
        $sampledPercentage.text(sampledPercentage.toFixed(0));

        $passingRate.text(segmentList.passingRate);
        events.emit('segmentsUpdated', getSegmentsString());
    }

    function _bindEvents() {
        events.on('waveReady', _setTotalDuration);
        events.on('widthUpdated', _setWidth);
        events.on('cursorDropped', _createSegments);

        events.on('contentValueChanged', _updateContent);
        events.on('labelValueChanged', _updateLabel);

        events.on('isPlaying', _updatePlayingFlag);
        events.on('qcErrorChanged', _updateQcErrors);

        events.on('textSegmentClicked', _setText);

        $(document).on('click', '.segment', function() {
            events.emit('getPlayingFlag', true);

            selectedSegmentIndex = $(this).index();
            segmentList.selectSegment(selectedSegmentIndex);
            _render();

            events.emit('segmentSelected', {
                'mode': 'segmentSelected',
                'selectedSegmentIndex': selectedSegmentIndex,
                'segments': segmentList.segments,
                'totalDuration': totalDuration,
                'playingFlag': playingFlag,
                'error': segmentList._getErrorMessage()
            });
        });

        // Hotkey Events
        events.on('hotkeyDelete', _deleteCurrentSegment);
        events.on('hotkeySelectLeft', _selectLeft);
        events.on('hotkeySelectRight', _selectRight);
        // events.on('hotkeyResizeLeft', _resizeLeft);
        // events.on('hotkeyResizeRight', _resizeRight);
    }

    // function _resizeLeft(data) {
    //     if(segmentList.selectedSegmentIndex > 0) {
    //         if(segmentList.segments[segmentList.selectedSegmentIndex-1].getDuration() > 2) {
    //             segmentList.segments[segmentList.selectedSegmentIndex-1].end -= 1;
    //             segmentList.segments[segmentList.selectedSegmentIndex].start -= 1;
    //             _render();

    //             console.log(segmentList.segments)
    //         }
    //     }
    // }

    // function _resizeRight(data) {
    //     if(segmentList.selectedSegmentIndex < segmentList.segments.length-1) {
    //         if(segmentList.segments[segmentList.selectedSegmentIndex+1].getDuration() > 2) {
    //             segmentList.segments[segmentList.selectedSegmentIndex+1].start += 1;
    //             segmentList.segments[segmentList.selectedSegmentIndex].end += 1;
    //             _render();

    //             console.log(segmentList.segments)
    //         }
    //     }
    // }

    function _selectLeft(data) {
        let shifted = false;
        if(segmentList.selectedSegmentIndex > 0) {
            segmentList.selectSegment(segmentList.selectedSegmentIndex-1);
            shifted = true;
        }

        if(shifted) {
            _render();
            events.emit('segmentSelected', {
                'mode': 'segmentSelected',
                'selectedSegmentIndex': segmentList.selectedSegmentIndex,
                'segments': segmentList.segments,
                'totalDuration': totalDuration,
                'playingFlag': playingFlag
            });
        }
    }

    function _selectRight(data) {
        let shifted = false;
        if(segmentList.selectedSegmentIndex < segmentList.segments.length - 1) {
            segmentList.selectSegment(segmentList.selectedSegmentIndex+1);
            shifted = true;
        }

        if(shifted) {
            _render();
            events.emit('segmentSelected', {
                'mode': 'segmentSelected',
                'selectedSegmentIndex': segmentList.selectedSegmentIndex,
                'segments': segmentList.segments,
                'totalDuration': totalDuration,
                'playingFlag': playingFlag
            });
        }
    }

    function _deleteCurrentSegment(data) {
                    
        if(!segmentList.deleteCurrentSegment()) {
            alert("You can't delete the first segment");
            return false;
        }

        _render();

        events.emit('segmentSelected', {
            'mode': 'segmentSelected',
            'selectedSegmentIndex': segmentList.selectedSegmentIndex,
            'segments': segmentList.segments,
            'totalDuration': totalDuration,
            'playingFlag': playingFlag
        });
    }

    function _setText(data) {
        let tokens = data.split(":");
        let label = tokens[0];
        switch(label) {
            case 'M1':
                label = 'A_male';
                break;
            case 'M2':
                label = 'B_male';
                break;
            case 'F1':
                label = 'A_female';
                break;
            case 'F2':
                label = 'B_female';
                break;
        }
        let content = tokens[1].trim();
        
        if(segmentList.selectedSegmentIndex !== -1) {
            _updateLabel(label);
            _updateContent(content);
            _render();
        }
    }

    function _updateQcErrors(data) {
        segmentList.updateQcErrors(data);
        _render();
    }

    function _updatePlayingFlag(flag) {
        playingFlag = flag;
    }

    function _enableHandles() {

        $('.handle').each(function(index) {

            var containmentX1 = $(this).parent().offset().left + 20;
            var containmentY1 = $(this).parent().offset().top;

            var containmentX2 = 0;
            var containmentY2 = $(this).parent().offset().top;
            var rightSegment = $(this).parent().next().find('.handle');
            
            if($(rightSegment).offset()) {
                var ha = $(rightSegment).offset().left - 10;
                containmentX2 = $(rightSegment).offset().left - 10;
            } else {
                // containmentX2 = $(this).parent().parent().children('.segment').first().offset().left + $(this).parent().parent().width();
                // var t1 = $(this).parent().parent().children('.segment').first().offset().left;
                // var t2 = $(this).parent().parent().width();
                // containmentX2 = containmentX1 + 10;
                
                containmentX2 = $(this).parent().offset().left + $(this).parent().width() + $(this).parent().next().width() - 10;
                console.log("x2 = " + containmentX2)
            }

            $(this).draggable({
                axis: "x",
                containment: [containmentX1, containmentY1, containmentX2, containmentY2],
                start: function(event, ui) {
                    
                    draggedSegmentIndex = $(this).parent().index();
                    $(this).css('background-color', 'red');
                    $(this).find('.overlay-handle').removeClass('hidden');
                },
                stop: function(event, ui) {
                    $(this).css('background-color', 'black');
                    $(this).find('.overlay-handle').addClass('hidden');

                    ui.position.left -= 6;
                    ui.originalPosition.left -= 6;
                    
                    var segmentLength = (segmentList.segments[draggedSegmentIndex].end -
                                        segmentList.segments[draggedSegmentIndex].start);   // 20
                    var percentage = segmentLength / ui.originalPosition.left;

                    var newEnd = ui.position.left * percentage;
                    newEnd += segmentList.segments[draggedSegmentIndex].start
                    
                    if(newEnd <= segmentList.segments[draggedSegmentIndex].start ||
                        newEnd >= segmentList.segments[draggedSegmentIndex+1].end ||
                        segmentList.segments[draggedSegmentIndex+1].end - newEnd < 0.7) {
                            // don't proceed with updating the segments
                    } else {
                        segmentList.segments[draggedSegmentIndex].end = newEnd;
                        segmentList.segments[draggedSegmentIndex+1].start = newEnd;

                        events.emit('segmentsResized', {
                            'mode': 'segmentsResized',
                            'updatedIndex': draggedSegmentIndex,
                            'segments': segmentList.segments,
                            'selectedSegmentIndex': segmentList.selectedSegmentIndex
                        });

                        segmentList.computePassingRate();
                    }
                    
                    _render();
                }
            });
        });
    }

    function _setWidth(width) {
        segmentListWidth = width;
        _render();
    }

    function _updateContent(value) {
        segmentList.setContent(value);
        events.emit('contentUpdated', segmentList);
        events.emit('errorUpdated', segmentList._getErrorMessage());
        _render();
    }

    function _updateLabel(value) {
        segmentList.setLabel(value);
        events.emit('contentUpdated', segmentList);
        events.emit('errorUpdated', segmentList._getErrorMessage());
        _render();
    }

    function _setTotalDuration(duration) {
        totalDuration = duration;
    }

    function _createSegments(cursorData) {
        segmentList.addSegments(cursorData);
        _render();
    }

    function _createSegmentHTML(segment, index) {
        var width = (segment.end - segment.start) / totalDuration * 100;
        var backgroundColor = segmentList.selectedSegmentIndex === index ? "yellow" : "#fff";

        var errorClass = segment.withError ? 'error' : '';
        var tested = '';

        if(qc !== '') {
            if(!segment.tested) {
                tested = 'not-tested';
            }
        }

        let label = '';
        if(segment.label) {
            label = segment.label;
            if(segment.label !== 'C') {
                label = segment.label.split('_');
                let speakerNumber = label[0] === 'A' ? '1' : '2';
                let speakerLetter = label[1] === 'male' ? 'M' : 'F';
                label = speakerLetter + speakerNumber;
            }
        }

        let handle = '';
        if(index < segmentList.segments.length - 1) {
            handle = '<div class="handle"><div class="overlay-handle hidden"></div></div>';
        }

        var html = [
            '<div class="segment ' + tested + ' ' + errorClass + '" style="width: ' + width + '%; background-color: ' + backgroundColor + '">',
                '<div class="values">',
                    '<h4 class="content-value">' + segment.content + '</h4>',
                    '<p class="label-value">' + label + '</p>',
                '</div>',
                handle,
            '</div>',
        ];

        return html.join('');
    }

    function getSegmentsString() {
        return segmentList.exportSegments();
    }

    return {
        'getSegmentsString': getSegmentsString
    }
})();