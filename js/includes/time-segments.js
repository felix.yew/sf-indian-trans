
class TimeSegment {

    constructor(start, end) {
        this.id = this._makeId();
        this.start = start;
        this.end = end;
        this.duration = end - start;
    }

    update(start, end) {
        if(start === null) {
            this.end = end;
        } else if(end === null) {
            this.start = start;
        }
        this.duration = this.end - this.start;
    }

    _makeId() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
      
        for (var i = 0; i < 5; i++)
          text += possible.charAt(Math.floor(Math.random() * possible.length));
      
        return text;
    }

    export() {
        return {
            'id': this.id,
            'start': this.start,
            'end': this.end,
            'duration': this.duration
        }
    }
}

class TimeSegmentList {

    constructor() {
        this.$timeList = $('#timeList');
        this.$totalDurationHandle = $('#totalDurationHandle');
        this.$totalDuration = $('#totalDuration');

        this.segments = [];
        this.playingSegmentIndex = -1;
    }

    setTwoValues(timePoint, totalDuration) {
        this.segments = [
            new TimeSegment(0, timePoint),
            new TimeSegment(timePoint, totalDuration)
        ];
    }

    setThreeValues(segments, selectedSegmentIndex, totalDuration) {
        this.segments = [];
        if(selectedSegmentIndex === 0) {
            this.segments = [
                new TimeSegment(0, segments[0].end),
                new TimeSegment(segments[0].end, totalDuration)
            ];
        } else if (selectedSegmentIndex === segments.length-1) {
            this.segments = [
                new TimeSegment(0, segments[selectedSegmentIndex].start),
                new TimeSegment(segments[selectedSegmentIndex].start, totalDuration)
            ];
        } else {
            this.segments = [
                new TimeSegment(0, segments[selectedSegmentIndex].start),
                new TimeSegment(segments[selectedSegmentIndex].start, segments[selectedSegmentIndex].end),
                new TimeSegment(segments[selectedSegmentIndex].end, totalDuration)
            ];
        }
    }
    
    updateValues(segments, updatedIndex, selectedSegmentIndex) {
        if((updatedIndex+1) < this.segments.length) {
            this.segments[updatedIndex].update(null, segments[updatedIndex].end);
            this.segments[updatedIndex+1].update(segments[updatedIndex+1].start, null);
        } else {

        }
    }

    exportTimeSegments() {
        let output = [];

        this.segments.forEach(function(segment) {
            output.push(segment.export());
        });

        return output;
    }
}

var timeSegmentsHandler = (function() {

    var timeSegmentsList = null;
    var totalDuration = -1;
    
    var playedSegmentId = -1;
    var timeListWidth = -1;
    var directPlay = false;

    var $timeList = $('#timeList');

    _init();

    function _init() {
        timeSegmentsList = new TimeSegmentList();

        _bindEvents();
    }

    function _render() {
        if(timeListWidth !== -1) {
            $timeList.css('width', timeListWidth);
        }

        var timesHTML = [];
        var i = 0;
        timeSegmentsList.segments.forEach(function(segment) {
            timesHTML.push(_createTimeHTML(segment));
        });
        $timeList.html(timesHTML);
        justRendered = false;
    }

    function _bindEvents() {
        events.on('cursorMoved', _updateTimeSegments);
        events.on('segmentSelected', _updateTimeSegments);
        events.on('segmentsResized', _updateTimeSegments);
        events.on('widthUpdated', _setWidth);

        $(document).on('click', '.time', function() {
            directPlay = true;

            let index = $(this).index();
            let currentId = timeSegmentsList.segments[index].id;
            if(currentId !== playedSegmentId) {
                events.emit('playAudio', {
                    'mode': 'region',
                    'isNew': true,
                    'timeSegment': timeSegmentsList.segments[index]
                });
                timeSegmentsList.playingSegmentIndex = index;
            } else {
                events.emit('playAudio', {
                    'mode': 'region',
                    'isNew': false,
                    'timeSegment': timeSegmentsList.segments[index]
                });
            }
            playedSegmentId = currentId;
        });
    }

    function _setWidth(width) {
        timeListWidth = width;
        _render();
    }

    function _updateTimeSegments(data) {
        if(directPlay) {
            directPlay = false;
            return false;
        }

        mode = data.mode;

        if(mode === 'cursorMoved' && !data.segmentSelection) {
            timePoint = data.timePoint;
            totalDuration = data.totalDuration;
            timeSegmentsList.setTwoValues(timePoint, totalDuration);
        } else if(mode === 'segmentSelected') {
            segments = data.segments;
            selectedSegmentIndex = data.selectedSegmentIndex;
            totalDuration = data.totalDuration;
            timeSegmentsList.setThreeValues(segments, selectedSegmentIndex, totalDuration);
        } else if(mode === 'segmentsResized') {
            segments = data.segments;
            updatedIndex = data.updatedIndex;
            selectedSegmentIndex = data.selectedSegmentIndex;
            timeSegmentsList.updateValues(segments, updatedIndex, selectedSegmentIndex);
        }

        _render();
    }

    function _createTimeHTML(segment) {
        var width = (segment.end - segment.start) / totalDuration * 100;
        var html = [
            '<div class="time" style="width: ' + width + '%;">',
                segment.duration.toFixed(4),
            '</div>',
        ];

        return html.join('');
    }

    function getTimeSegmentsString() {
        return timeSegmentsList.exportTimeSegments();
    }

    return {
        'getTimeSegmentsString': getTimeSegmentsString
    }
})();