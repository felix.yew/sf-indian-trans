class Cursor {

    constructor(timePoint, totalDuration) {
        this.$cursor = $('#cursor');
        this.$dropCursor = $('#dropCursor');

        this.timePoint = timePoint;
        this.totalDuration = totalDuration;
    }

    hideCursor() {
        this.$cursor.addClass('hidden');
    }

    showCursor() {
        this.$cursor.removeClass('hidden');
        this.$dropCursor.removeClass('hidden');
    }

    moveCursor(timePoint) {
        this.timePoint = timePoint;
    }

    getTimePoint() {
        return this.timePoint;
    }

    adjustPosition() {
        let leftValue = ((this.timePoint / this.totalDuration * 100) - 0.03) + '%';
        this.$cursor.css('left', leftValue);
    }

    dropCursor() {
        this.hideCursor();
        events.emit('cursorDropped', {
            'timePoint': this.timePoint,
            'totalDuration': this.totalDuration
        });
    }
}