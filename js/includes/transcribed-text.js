var transcribed_text = (function() {

    var textSegments = [];
    var activeSegment = -1;
    var $transcribedText = $('#transcribedText');

    _init();

    function _init() {
        let transcribedText = $transcribedText.text().split('\n');
        if(_correctFormat(transcribedText)) {
            
            textSegments = transcribedText;

            _render();
            _bindEvents();
        }
    }

    function _correctFormat(transcribed_text) {
        if(transcribed_text.length < 4) {
            return false;
        } else if(_correctLabel(transcribed_text[0]) &&
            _correctLabel(transcribed_text[1]) &&
            _correctLabel(transcribed_text[2]) &&
            _correctLabel(transcribed_text[3])) {
            return true;
        }

        return false;
    }

    function _correctLabel(text) {
        return (text[0] == 'M' || text[0] == 'F') &&
            (text[1] == '1' || text[1] == '2');
    }

    function _render() {
        let textSegmentsHTML = [];
        for(let i = 0; i < textSegments.length; i++) {
            textSegmentsHTML.push(_createTextSegmentHTML(textSegments[i], i));
        }
        $transcribedText.html(textSegmentsHTML);
    }

    function _bindEvents() {
        $(document).on('click', '.text-segment', function() {
            activeSegment = $(this).index();
            events.emit('textSegmentClicked', textSegments[activeSegment]);
            _render();
        });
    }

    function _createTextSegmentHTML(textSegment, i) {
        let active = '';
        if(i === activeSegment) {
            active = 'active';
        }
        let html = [
            '<div class="text-segment ' + active + '">',
                '<p>' + textSegment + '</p>',
            '</div>'
        ];

        return html.join('');
    }
})();