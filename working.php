<?php 
	require_once('config.php');
	require_once('header.php');
?>

	<div class="row">
        <div class="col-md-3">
            <div class="transcribed-text">
                <p class="site-logo-container"><img class="site-logo" src="images/logo.png" /></p>
                <h3>Transcribed Text</h3>
                <p id="transcribedText"><?php echo $_SESSION['text']; ?></p>
                <!--<div id="transcribedText">M1: Hi, I am handsome.
M2: No. I am pretty
M1: No. I am pretty
M2: No. I am pretty</div>-->
            </div>
        </div>
        <div class="col-md-9">

            <div class="row audio-details">
                <div class="col-md-12">
                    <h4>Audio Details (<?php echo $_SESSION['mode']; ?>):</h4>
                    <button data-toggle="modal" data-target="#hotkeyHintsModal"><i class="fa fa-info-circle"></i></button>
                </div>
                <div class="col-md-12">
                    <p><span class="font-bold">Duration:</span> <span id="totalDuration"><?php echo $_SESSION['duration']; ?></span></p>
                    <p><span class="font-bold">Effective Duration:</span> <span id="effectiveDuration">00:00:00</span></p>
                    <p><span class="font-bold">Batch Name:</span> <?php echo $_SESSION['batch_name']; ?></p>
                    <p><span class="font-bold">User ID:</span> <?php echo $_SESSION['user_id']; ?></p>
                </div>
            </div>
            <?php if(isset($_SESSION['mode']) && ($_SESSION['mode'] == 'QC_LA' || $_SESSION['mode'] == 'FinalQC')): ?>
            <div class="row qc-details">
                <p><span class="font-bold">Sampled Segments: <span id="sampledSegments">0</span> / <span id="segmentsCount"></span></p>
                <p class="hidden"><span class="font-bold">Sampled Percentage: <span id="sampledPercentage">100</span>%</p>
                <p><span class="font-bold">Passing Rate: <span id="passingRate">100</span>%</p>
            </div>
            <?php endif; ?>
            <div class="row input-manager">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="labelValueInput">Segment Label:</label>
                        <select class="form-control label-value-input pull-left" id="labelValueInput">
                            <option value="">Select</option>
                            <option value="<?php echo $_SESSION['first_speaker']->value; ?>"><?php echo $_SESSION['first_speaker']->text; ?></option>
                            <option value="<?php echo $_SESSION['second_speaker']->value; ?>"><?php echo $_SESSION['second_speaker']->text; ?></option>
                            <option value="C">Invalid</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="contentValueInput">Segment Content:</label>
                        <textarea class="form-control content-value-input pull-left" type="text" id="contentValueInput" rows="2"></textarea>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="labelValueInput">Error message:</label>
                        <h4 class="text-success" id="errorMessage">No errors</h5>
                    </div>
                </div>
            </div>
            <?php if(isset($_SESSION['mode']) && ($_SESSION['mode'] == 'QC_LA' || $_SESSION['mode'] == 'FinalQC')): ?>
            <div class="row input-manager">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="labelValueInput">QC Problems:</label>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="qc_problems[]" value="Wrong Label">Wrong Label</label>
                                </div>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="qc_problems[]" value="Wrong Content">Wrong Content</label>
                                </div>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="qc_problems[]" value="Capitalization">Capitalization</label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="qc_problems[]" value="Segment Length">Segment Length</label>
                                </div>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="qc_problems[]" value="Punctuation">Punctuation</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endif; ?>

            <div class="row">
                <div class="col-md-12">
                    <div class="transcriber" id="transcriber">
                        <div class="wave-form" id="waveform"></div>
                        <div class="segment-list" id="segmentList">
                            <div class="cursor hidden" id="cursor">
                                <div class="drop-cursor" id="dropCursor"></div>
                            </div>
                            <div class="actual-list" id="actualList">
                            </div>
                        </div>
                        <div class="time-list" id="timeList">
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="controls">
                        <h4>Controls:</h4>
                        <table>
                            <tr>
                                <td class="left-value">Magnifier</td>
                                <td>
                                    <i class="icon-zoom-out" id="zoomOut"></i>
                                    <i class="icon-zoom-in" id="zoomIn"></i>
                                </td>
                            </tr>
                            <tr>
                                <td class="left-value">Speed</td>
                                <td>
                                    <i class="fa fa-fast-backward" id="fastBackward"></i>
                                    <i class="fa fa-fast-forward" id="fastForward"></i>
                                    <span class="speed" id="speed">Normal</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="left-value">Save</td>
                                <td>
                                    <i class="fa fa-save" id="saveProgress"></i>
                                    <span class="save-status hidden" id="saveStatus"></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-md-4">
                    <button type="button" class="play-pause" id="playPauseButton">Play</button>
                </div>
                <div class="col-md-4">
                    <button type="button" class="submit-batch pull-right" id="submitBatch">Submit</button>
                    <?php if(isset($_SESSION['mode']) && ($_SESSION['mode'] == 'QC_LA' || $_SESSION['mode'] == 'FinalQC')): ?>
                        <button type="button" class="fail-batch pull-right" id="failBatch">Fail</button>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="hotkeyHintsModal" class="modal fade" role="dialog" aria-hidden="true">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Hotkeys</h4>
            </div>
            <div class="modal-body">
            <table class="table">
                <thead>
                    <tr>
                        <th>Action</th>
                        <th>Hotkey</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Delete Segment</td>
                        <td>ALT + Backspace</td>
                    </tr>
                    <tr>
                        <td>Zoom In</td>
                        <td>ALT + i</td>
                    </tr>
                    <tr>
                        <td>Zoom Out</td>
                        <td>ALT + o</td>
                    </tr>
                    <tr>
                        <td>Select Left Segment</td>
                        <td>ALT + Arrow Left</td>
                    </tr>
                    <tr>
                        <td>Select Right Segment</td>
                        <td>ALT + Arrow Right</td>
                    </tr>
                </tbody>
            </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            </div>

        </div>
    </div>
<?php require_once('footer.php'); ?>
	