<?php 
	require_once('config.php');
	require_once('functions/utils.php');

	require_once('header.php');
?>
	<div class="login">
		<p class="site-logo-container"><img class="site-logo" src="images/logo.png" /></p>

  		<div class="login-triangle"></div>
  
  		<h2 class="login-header">LOGIN</h2>

		<form class="login-container" method="POST" id="loginForm">
			<p><input type="text" placeholder="Username" id="username" required /></p>
            <p><input type="password" placeholder="Password" id="password" required /></p>
            <p><input type="submit" value="Login" id="login" /></p>
			<p class="text-danger form-error hidden" id="loginError">Invalid username/password</p>
  		</form>
	</div>

<?php require_once('footer.php'); ?>
	