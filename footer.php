
    </div>
    <script type="text/javascript" src="libs/jquery/jquery.min.js"></script>
    <script type="text/javascript">
        // var ROOT = '/WebApp/transcriber/';
        // var ROOT = '/transcriber/';
        var ROOT = '/';
        var qc = '';
        <?php if(isset($_SESSION['mode']) && $_SESSION['mode'] == 'QC_LA'): ?>
            qc = 'qc.php';
        <?php elseif(isset($_SESSION['mode']) && $_SESSION['mode'] == 'FinalQC'): ?>
            qc = 'final-qc.php';
        <?php endif; ?>

        var audio = '';
        <?php if(isset($_SESSION['filename'])): ?>
            audio = 'audio/<?php echo $_SESSION['filename']; ?>';
        <?php endif; ?>

        var taskID = '';
        <?php if(isset($_SESSION['task_id'])): ?>
            taskID = '<?php echo $_SESSION['task_id']; ?>';
        <?php endif; ?>

        var userID = '';
        <?php if(isset($_SESSION['user_id'])): ?>
            userID = '<?php echo $_SESSION['user_id']; ?>';
        <?php endif; ?>

        var batchName = '';
        <?php if(isset($_SESSION['batch_name'])): ?>
            batchName = '<?php echo $_SESSION['batch_name']; ?>';
        <?php endif; ?>

        var workingKey = taskID + '-' + userID + '-' + batchName;
        var isAdmin = false;
        <?php if(isset($_SESSION['is_admin'])): ?>
            isAdmin = '<?php echo $_SESSION['is_admin']; ?>' === '1';
        <?php endif; ?>
        
        <?php if(isset($_SESSION['files']) && $_SESSION['files']): ?>
            var label_map = [];
            label_map['male'] = 'M';
            label_map['female'] = 'F';
            label_map['invalid'] = 'I';
            label_map['No speech'] = 'I';

            var initialData = `<?php echo $_SESSION['files']->text; ?>`;
            initialData = initialData.split("\n");

            var list = [];
            var savedList = localStorage.getItem(workingKey, null);
            savedList = savedList ? JSON.parse(savedList) : [];            

            let j = 0;

            let qcErrorsList = '<?php echo $_SESSION['qc_reasons']; ?>';
            if(qcErrorsList && qcErrorsList.text) {
                qcErrorsList = JSON.parse(qcErrorsList.text);
                qcErrorsList = qcErrorsList.Segment;
            } else {
                qcErrorsList = null;
            }
            
            for(let i = 0; i < initialData.length; i += 6, j++) {
                let start = end = content = label = '';

                let time = initialData[i].split(":")[1];
                time = time.split("_");
                start = parseFloat(time[0]) / 1000;
                end = parseFloat(time[1]) / 1000;

                let type = time[2][0];
                
                if(type !== 'C') {
                    content = initialData[i+1].split(":")[1];
                }
                
                try {
                    label = time[2] + '_' + time[3].split('.')[0];
                } catch(err) {
                    label = 'C';
                }
                
                let oStart = start;
                let oEnd = end;
                let oContent = content;
                let oLabel = label_map[label];
                let oQcErrors = qcErrorsList ? qcErrorsList[j].QCReasons.join(',') : '';
                let oTested = false;
                if(savedList[j]) {
                    oContent = savedList[j].content ? savedList[j].content : content;
                    oLabel = savedList[j].label ? savedList[j].label : label;
                    oTested = savedList[j].tested;
                    oQcErrors = savedList[j].qcErrors ? savedList[j].qcErrors : '';
                }
                if(typeof oLabel === 'undefined') {
                    oLabel = label;
                }
                
                list.push({
                    'start': oStart,
                    'end': oEnd,
                    'content': oContent,
                    'label': oLabel,
                    'tested': oTested,
                    'qcErrors': oQcErrors
                })
            }
            
            if(j < savedList.length) {
                list[list.length-1].end = savedList[j].start;
                for( ; j < savedList.length; j++) {
                    list.push({
                        'start': savedList[j].start,
                        'end': savedList[j].end,
                        'content': savedList[j].content,
                        'label': savedList[j].label,
                        'tested': savedList[j].tested,
                        'qcErrors': savedList[j].qcErrors
                    })
                }
            }
            
            console.log(list);
            localStorage.setItem(workingKey, JSON.stringify(list));
        <?php endif; ?>
    </script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <?php if(strpos(basename($_SERVER['PHP_SELF']), 'working') !== false) : ?>
    <!-- Pubsub 3rd party module -->
    <script type="text/javascript" src="js/includes/pubsub.js"></script>

    <!-- Custom classes -->
    <script type="text/javascript" src="js/includes/input-manager.js"></script>
    <script type="text/javascript" src="js/includes/magnifier.js"></script>
    <script type="text/javascript" src="js/includes/cursor.js"></script>
    <script type="text/javascript" src="js/includes/wave-form.js"></script>
    <script type="text/javascript" src="js/includes/segments.js"></script>
    <script type="text/javascript" src="js/includes/time-segments.js"></script>
    <script type="text/javascript" src="js/includes/action-buttons.js"></script>
    <script type="text/javascript" src="js/includes/transcribed-text.js"></script>
    <script type="text/javascript" src="js/includes/audio-details.js"></script>
    <script type="text/javascript" src="js/includes/progress-saver.js"></script>
    <script type="text/javascript" src="js/includes/hotkey.js"></script>
    <?php endif; ?>

    <!-- Main JS File -->
    <script type="text/javascript" src="js/main.js"></script>
</body>
</html>