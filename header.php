<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="description" content="Web-based audio transcriber">
    <meta name="author" content="JudeMaranga">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Transcriber</title>
    
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="libs/bootstrap/css/bootstrap.min.css" />
    <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/main.css?v=1.0" />

    <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>
    <script src="https://unpkg.com/wavesurfer.js"></script>
    <script src="https://unpkg.com/wavesurfer.js/dist/plugin/wavesurfer.regions.min.js"></script>
</head>

<body>
    <div class="container-fluid">
        <?php if(strpos(basename($_SERVER['PHP_SELF']), 'working') !== false) : ?>
            <div class="preloader">
                <div class="wrap">
                    <div class="loading">
                        <div class="bounceball"></div>
                        <div class="text">NOW LOADING</div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        