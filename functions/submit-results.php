<?php

require_once('utils.php');

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

$final_data = json_decode($_POST['data']);
$pass_or_fail = isset($_POST['pass_or_fail']) ? $_POST['pass_or_fail'] : true;
$effective_duration = isset($_POST['effective_duration']) ? $_POST['effective_duration'] : '';

$file_path = create_file($final_data->segments);
$qc_file_path = create_qc_file($effective_duration, $final_data->segments);

$response = submit_batch($file_path, $qc_file_path, $pass_or_fail);

if($response->status == "True" || strpos($response->message, 'Status:Completed') !== false) {
	// delete the output file
    if(file_exists($file_path)) {
        unlink($file_path);
    }

    // delete the qc file path
    if(file_exists($qc_file_path)) {
        unlink($qc_file_path);
    }

    // delete the audio file
    if(file_exists($_SESSION['file_location'])) {
        unlink($_SESSION['file_location']);
    }

	clear_session();

	echo 'success';

} else {
	echo $response->message;
}

function create_file($data) {
    $label_map = array(
        'A_male' => 'male',
        'B_male' => 'male',
        'A_female' => 'female',
        'B_female' => 'female',
        'C' => 'bad-audio,no-speech',
    );

    $output = '';

    for($i = 0; $i < count($data); $i++) {
        $start_time = parse_time($data[$i]->start);
        $end_time = parse_time($data[$i]->end);

        $type = $data[$i]->label;

        $transcription = '坏';
        if($type != 'C') {
            $transcription = $data[$i]->content;
        }

        $label = $label_map[$data[$i]->label];

        $output .= "FILE:" . $start_time . '_' . $end_time . '_' . $type . '.wav(检查结果)';
        $output .= "\nTRANSCRIPTION:" . $transcription;
        $output .= "\nTRANSCRIPTION:";
        $output .= "\nLABELS:" . $label;
        $output .= "\nTRANSCRIPTION:";

        if($i < count($data)-1) {
            $output .= "\n\n";
        }
    }

    // Write to the actual file
    $file_path = dirname(dirname(__FILE__)) . '/output/' . $_SESSION['output_file'];
    $file = fopen($file_path, "wb");
    fwrite($file, $output);
    fclose($file);

    return $file_path;
}

function create_qc_file($effective_duration, $data) {
    $end_time = microtime(true);
    $processing_time = gmdate("H:i:s", $end_time - $_SESSION['start_time']);
    $segments = [];
    
    for($i = 0; $i < count($data); $i++) {
        array_push($segments, array(
            'name' => ('Segment' . ($i + 1)),
            'QCReasons' => explode(',', $data[$i]->qcErrors)
        ));
    }

    $output = array(
        'processingTime' => $processing_time,
        'effectiveDuration' => $effective_duration,
        'Segment' => $segments
    );

    // Write to the actual file
    $qc_file_path = dirname(dirname(__FILE__)) . '/output/qc__' . $_SESSION['output_file'];
    $file = fopen($qc_file_path, "wb");
    fwrite($file, json_encode($output));
    fclose($file);

    return $qc_file_path;
}

function parse_time($time) {
    $time = round($time, 3);
    $time *= 1000;
    
    $time = strval($time);

    return str_pad($time, 7, '0', STR_PAD_LEFT);
}


function submit_batch($file_path, $qc_file_path, $pass_or_fail) {

    // TODO: Use this in the name header
    $filename = $_SESSION['output_file'];

    $curl = curl_init();

    if (function_exists('curl_file_create')) { // php 5.5+
        $cFile = curl_file_create($file_path, 'text/plain', 'data');
        $qc_cFile = curl_file_create($qc_file_path, 'text/plain', 'data');
	} else {
		$cFile = '@' . realpath($file_path);
		$qc_cFile = '@' . realpath($qc_file_path);
    }

    $text_filename = $_SESSION['batch_name'] . '.txt';
    $post = array(
        $text_filename => $cFile,
        'QCReasons' => $qc_cFile
    );

    $headers = array(
        "taskid: " . $_SESSION['task_id'],
        "userId: " . $_SESSION['user_id']
    );

    if($_SESSION['mode'] == 'QC_LA' || $_SESSION['mode'] == 'FinalQC') {
        array_push($headers, "PassOrFail: " . $pass_or_fail);
    }
    
    $server = $_SESSION['server'];
    $project_name = $_SESSION['project_name'];
    $api_url_root = 'http://112.74.135.127';
    if($server == 'BG') {
        $api_url_root = 'http://122.152.55.205:8080';
    } else if($server == 'Dongguan') {
        $api_url_root = 'http://116.6.15.121';
    }

    curl_setopt_array($curl, array(
        CURLOPT_URL => "$api_url_root/Yueyu/api/XunFeiVoice/SubmitBatch",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_POST => true,
        CURLOPT_POSTFIELDS => $post,
        CURLOPT_HTTPHEADER => $headers
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    return json_decode($response);
}

?>