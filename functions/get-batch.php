<?php

require_once('mp3-file.php');

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

// TODO: 
//	1. call get batch API here
//	2. store values in session
$user_id = isset($_POST['user_id']) ? $_POST['user_id'] : '';
$project_name = isset($_POST['project_name']) ? $_POST['project_name'] : '';
$mode = isset($_POST['mode']) ? $_POST['mode'] : '';
$server = isset($_POST['server']) ? $_POST['server'] : '';

$api_url_root = 'http://112.74.135.127';
if($server == 'BG') {
    $api_url_root = 'http://122.152.55.205:8080';
} else if($server == 'Dongguan') {
    $api_url_root = 'http://116.6.15.121';
}

$result = FALSE;
$output = null;
if($mode == 'Key') {
    $result = send_request("$api_url_root/Yueyu/api/XunFeiVoice/GetWorkingBatch?userId=$user_id&projectName=$project_name&taskName=Check");
    $output = json_decode($result);
}
if($result === FALSE || ($output && !$output->taskid)) {
    $result = send_request("$api_url_root/Yueyu/api/XunFeiVoice/GetWorkingBatch?userId=$user_id&projectName=$project_name&taskName=$mode");
} else if($output && $output->taskid) {
    $mode = 'Check';
}

if($result !== FALSE) {
	$result = json_decode($result);

	if($result->status != "OK") {
        echo 'batch_not_ok';
		die();
	}

    $_SESSION['filename'] = $result->taskid . '-' . $user_id . '-' . $result->batchName . '.mp3';
    $_SESSION['output_file'] = $result->taskid . '-' . $user_id . '-' . $result->batchName . '.txt';
    $file_location = download_file($result->url, $_SESSION['filename']);
    
    $_SESSION['text'] = $result->text;
    $_SESSION['file_location'] = $file_location;
    $_SESSION['task_id'] = $result->taskid;
    $_SESSION['batch_name'] = $result->batchName;
    $_SESSION['user_id'] = $user_id;
    $_SESSION['mode'] = $mode;
    $_SESSION['files'] = isset($result->files) ? $result->files[0] : null;
    $_SESSION['qc_reasons'] = isset($result->files) ? json_encode($result->files[1]) : null;
    $_SESSION['duration'] = get_duration($file_location);
    $_SESSION['server'] = $server;
    $_SESSION['project_name'] = $project_name;
    $_SESSION['start_time'] = microtime(true);
    $_SESSION['is_admin'] = strpos($user_id, '_test') !== false;

    set_speakers();
    
	echo 'success';
} else {
	echo 'failed';
	die();
}

function set_speakers() {
    $label_options = explode('_', $_SESSION['batch_name']);
    $first_speaker_token = $label_options[1];
    $second_speaker_token = $label_options[2];
    $first_speaker = new stdClass();
    $second_speaker = new stdClass();

    if($first_speaker_token == 'M') {
        $first_speaker->value = 'A_male';
        $first_speaker->text = 'Male1';
    } else if($first_speaker_token == 'F') {
        $first_speaker->value = 'A_female';
        $first_speaker->text = 'Female1';
    }

    if($second_speaker_token == 'M') {
        $second_speaker->value = 'B_male';
        $second_speaker->text = 'Male2';
    } else if($second_speaker_token == 'F') {
        $second_speaker->value = 'B_female';
        $second_speaker->text = 'Female2';
    }

    $_SESSION['first_speaker'] = $first_speaker;
    $_SESSION['second_speaker'] = $second_speaker;
}

function send_request($url) {
    // create curl resource 
    $ch = curl_init(); 

    // set url 
    curl_setopt($ch, CURLOPT_URL, $url); 

    //return the transfer as a string 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
    curl_setopt($ch, CURLOPT_HEADER, 0);

    // $output contains the output string 
    $output = curl_exec($ch); 

    // close curl resource to free up system resources 
    curl_close($ch);

    return $output;
}

function get_duration($file) {
    $mp3file = new MP3File($file);

    return MP3File::formatTime($mp3file->getDurationEstimate());
}

function download_file($url, $filename) {
    $file_location = dirname(dirname(__FILE__)) . "/audio/$filename";

    $url = explode('/', $url);
    $url[2] = '122.152.55.205:8080';
    $url = implode('/', $url);
    file_put_contents($file_location, fopen(str_replace(" ", "%20", $url), 'r'));

    return $file_location;
}

?>