<?php
	function clear_session() {
		$login = isset($_SESSION['login']) && $_SESSION['login'];

		if(strpos(basename($_SERVER['PHP_SELF']), 'login')) {
			exit;
		}

		if(!isset($_SESSION['login'])) {
			if(strpos(basename($_SERVER['PHP_SELF']), 'login') === false) {
				session_start();
				$_SESSION['next'] = $_SERVER['PHP_SELF'];
				header('Location: /login.php');
				exit;
			}
		}

		//remove PHPSESSID from browser
		if(isset($_COOKIE[session_name()])) {
			setcookie(session_name(), '', time() - 3600, '/');
		}

		//clear session from globals
		$_SESSION = array();

		//clear session from disk
		session_destroy();

		if($login) {
			$_SESSION['login'] = true;
		}
	}
?>