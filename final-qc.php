<?php 
	require_once('config.php');
	require_once('functions/utils.php');
	clear_session();

	require_once('header.php');
?>

	<div class="login">
		<p class="site-logo-container"><img class="site-logo" src="images/logo.png" /></p>

  		<div class="login-triangle"></div>
  
  		<h2 class="login-header">Get Batch (Final QC)</h2>

		<form class="login-container" method="POST" id="getBatchForm">
			<p>
				<span class="radio-input-container"><input type="radio" name="server" value="BG" checked /> <label class="radio-label">BG</label></span>
				<span class="radio-input-container"><input type="radio" name="server" id="server" value="Dongguan" /> <label class="radio-label">Dongguan</label></span>
				<span class="radio-input-container"><input type="radio" name="server" value="Testing" /> <label class="radio-label">Testing</label></span>
			</p>
			<p><select id="projectName" required />
				<option value="">Select Project Name</option>
				<option value="PXunFeiVoice">PXunFeiVoice</option>
				<option value="PXFIndian_BG">PXFIndian_BG</option>
				<option value="PXFIndian_PH">PXFIndian_PH</option>
				<option value="PXFSea_BG">PXFSea_BG</option>
				<option value="PXFSea_PH">PXFSea_PH</option>
				<option value="PXFSea_ID">PXFSea_ID</option>
				<option value="PXFSea_TH">PXFSea_TH</option>
				<option value="PXFSea_VN">PXFSea_VN</option>
				<option value="PXF_PH">PXF_PH</option>
			</select></p>
			<p><input type="text" placeholder="User ID" id="userId" required /></p>
			<p><input type="submit" value="Get Batch" id="getBatch" /></p>
			<p class="text-danger form-error hidden" id="batchError">Error: Invalid User ID</p>
            <input type="hidden" class="mode" value="FinalQC" />
  		</form>
	</div>

<?php require_once('footer.php'); ?>
	