FILES:
- audio/ -> this is where the downloaded audio files (when getting batch) will be temporarily stored
- css/ -> contains the CSS files for the web application
- functions/ -> contains helper files which contains helper functions for the project
    - get-batch.php -> used for getting the batch from the server
    - login-auth.php -> used for authenticating the user in the login page
    - mp3-file.php -> contains helper class for MP3 file conversion
    - submit-results.php -> used for submitting the batch
    - utils.php -> contains other utility functions
- images/ -> contains image assets needed for the web application
- js/ -> contains the JS files for the web application
    - main.js -> this is the very first JS file that is being used when the user hasn't logged in yet and/or gotten a batch yet
    - includes/ -> contains all the custom JS modules for this web application
        - action-buttons.js -> for all the action buttons in the web application like the SUBMIT and FAIL buttons
        - audio-details.js -> displays all the details of the audio at the top bar
        - cursor.js -> contains a class representation of the cursor (the movable vertical bar found in the audio wave)
        - hotkey.js -> manages all the hotkey functions in the web application
        - input-manager.js -> manages the values of all the inputs/dropdowns in this web application
        - magnifier.js -> for the zooming in and zooming out of the audio
        - progress-saver.js -> saves the current progress of the user in the localstorage
        - pubsub.js -> a 3rd party library for managing the overall interaction of the modules
        - segments.js -> manages the segments in the audio wave
        - time-segments.js -> manages the time segments in the audio wave
        - transcribed-text.js -> manages the transcription text at the left side of the screen
        - wave-form.js -> manages the audio wave in the middle of the screen
- libs/ -> contains 3rd party libraries needed for the application
- output/ -> this is where the output .txt files are temporarily stored
- config.php -> contains the configuration of the whole web application
- final-qc.php -> he page where you can get a batch in the Final QC Pool
- footer.php -> the footer file of the web application
- header.php -> the header file of the web application
- index.php -> the page where you can get a batch in the Key/Check Pool
- login.php -> the login page of the whole web application
- qc.php -> the page where you can get a batch in the QC Pool
- working.php -> the main working page


HOW THE PROGRAM WORKS:
1. Deploy the application by copying all the files to the server
2. Configure some paths in the code:
    - footer.php -> around lines 5 - 7, configure the ROOT path of the application
    - utils.php -> line 13, update the login redirect path accordingly
3. To access the following pages:
    - Key/Check -> Just type the web url of the root in the browser
    - QC -> Just type the web url of the root + '/qc.php' in the browser
    - FinalQC -> Just type the web url of the root + 'final-qc.php' in the browser
4. After performing step 3, you will be redirected to the login page first where you will need to login with the following hard coded credentials:
    username: admin
    password: qwerty123
5. After logging-in in step 4, you will be redirected to the corresponding get batch page
6. Select the site, the project name, and input the user ID and then click Get Batch
7. You will then be redirected to the working page where you can process the audio.


ADDITIONAL NOTES:
- This project needs PHP 7 to run
- This whole web application follows the PubSub pattern